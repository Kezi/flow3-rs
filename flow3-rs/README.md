## flow3-rs crate

This crate contains the main Board Support for the Flow3r.
It is in many parts built on async, so it needs an async runtime, however it should be runtime-independent by itself.
`flow3-rs-rt` provides an embassy-based runtime for use with this bsp-crate.
For most use-cases, you should use `flow3-rs` with the `flow3-rs-rt`.