use embassy_futures::select::{select, select3, Either, Either3};
use embassy_sync::{
    blocking_mutex::raw::CriticalSectionRawMutex,
    mutex::Mutex,
    pubsub::{PubSubChannel, Subscriber, WaitResult},
};
use embedded_hal_async::digital::Wait;
use esp_println::println;
use hal::{
    prelude::*,
    gpio::{Gpio0, Gpio3, Gpio8, Unknown},
    i2c::I2C,
    peripherals::I2C0,
};
use shared_bus::{I2cProxy, XtensaMutex};

static INPUTS_CHANNEL: PubSubChannel<CriticalSectionRawMutex, Flow3rInputEvent, 32, 32, 32> =
    PubSubChannel::<CriticalSectionRawMutex, Flow3rInputEvent, 32, 32, 32>::new();

static INPUT_STATE: Mutex<CriticalSectionRawMutex, [bool;6]> = Mutex::new([false;6]);
static PIN_LOCATIONS: [FlowerInputEventSource;6] = [FlowerInputEventSource::SW1Left, FlowerInputEventSource::SW1Right, FlowerInputEventSource::SW2Left, FlowerInputEventSource::SW2Right, FlowerInputEventSource::SW1Press, FlowerInputEventSource::SW2Press];

pub type InputEventListener =
    Subscriber<'static, CriticalSectionRawMutex, Flow3rInputEvent, 32, 32, 32>;

#[derive(Copy, Clone)]
pub struct Inputs;

impl Inputs {
    pub fn get_event_listener(
        &mut self,
    ) -> Result<InputEventListener, embassy_sync::pubsub::Error> {
        INPUTS_CHANNEL.subscriber()
    }

    pub fn split(self) -> InputParts {
        InputParts {
            sw1_left: Flow3rInput(FlowerInputEventSource::SW1Left, 0),
            sw1_center: Flow3rInput(FlowerInputEventSource::SW1Press, 4),
            sw1_right: Flow3rInput(FlowerInputEventSource::SW1Right, 1),
            sw2_left: Flow3rInput(FlowerInputEventSource::SW2Left, 2),
            sw2_center: Flow3rInput(FlowerInputEventSource::SW2Press, 5),
            sw2_right: Flow3rInput(FlowerInputEventSource::SW2Right, 3),
        }
    }
}

pub struct InputRunner {
    i2c: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
    i2c_int: Gpio8<Unknown>,
    sw1_button: Gpio0<Unknown>,
    sw2_button: Gpio3<Unknown>,
}

impl InputRunner {
    pub fn new(
        i2c: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
        i2c_int: Gpio8<Unknown>,
        sw1_button: Gpio0<Unknown>,
        sw2_button: Gpio3<Unknown>,
    ) -> Self {
        Self { i2c, i2c_int, sw1_button, sw2_button }
    }

    pub async fn run(self) -> ! {
        let mut i2c_int = self.i2c_int.into_pull_up_input();
        let mut sw1_button = self.sw1_button.into_pull_up_input();
        let mut sw2_button = self.sw2_button.into_pull_up_input();
    
        let mut pe2 = port_expander::Max7321::new(self.i2c, true, true, false, true);
    
        let pe2_io = pe2.split();
        let mut sw1_r = pe2_io.p0;
        sw1_r.set_high().unwrap();
        let mut sw1_l = pe2_io.p7;
        sw1_l.set_high().unwrap();
        let mut sw2_r = pe2_io.p5;
        sw2_r.set_high().unwrap();
        let mut sw2_l = pe2_io.p4;
        sw2_l.set_high().unwrap();
    
        let _lcd_rst = pe2_io.p3;
    
        let inputs_publisher = INPUTS_CHANNEL.immediate_publisher();

        let values = port_expander::read_multiple([&sw1_l, &sw1_r, &sw2_l, &sw2_r]).unwrap();
        {
            let mut input_state = INPUT_STATE.lock().await;
            input_state[0..4].copy_from_slice(&values);
        }
    
        loop {
            match select3(
                i2c_int.wait_for_low(),
                sw1_button.wait_for_any_edge(),
                sw2_button.wait_for_any_edge(),
            )
            .await
            {
                Either3::First(_) => {
                    let values =
                        port_expander::read_multiple([&sw1_l, &sw1_r, &sw2_l, &sw2_r]).unwrap();
                    let mut previous_inputs = INPUT_STATE.lock().await;
                    for (i, value) in values.into_iter().enumerate() {
                        
                        if previous_inputs[i] != value{
                            inputs_publisher.publish_immediate(Flow3rInputEvent {
                                input: PIN_LOCATIONS[i],
                                etype: if value {Flow3rInputEventType::Release} else {Flow3rInputEventType::Press},
                            });
                            previous_inputs[i] = value;
                        }
                    }
                }
                Either3::Second(_) => {
                    let mut input_state = INPUT_STATE.lock().await;
                    if sw1_button.is_low().unwrap() {
                        inputs_publisher.publish_immediate(Flow3rInputEvent {
                            input: FlowerInputEventSource::SW1Press,
                            etype: Flow3rInputEventType::Press,
                        });
                        input_state[4] = true;
                    } else {
                        inputs_publisher.publish_immediate(Flow3rInputEvent {
                            input: FlowerInputEventSource::SW1Press,
                            etype: Flow3rInputEventType::Release,
                        });
                        input_state[4] = false;
                    }
                },
                Either3::Third(_) => {
                    let mut input_state = INPUT_STATE.lock().await;
                    if sw2_button.is_low().unwrap() {
                        inputs_publisher.publish_immediate(Flow3rInputEvent {
                            input: FlowerInputEventSource::SW1Press,
                            etype: Flow3rInputEventType::Press,
                        });
                        input_state[5] = true;
                    } else {
                        inputs_publisher.publish_immediate(Flow3rInputEvent {
                            input: FlowerInputEventSource::SW1Press,
                            etype: Flow3rInputEventType::Release,
                        });
                        input_state[5] = false;
                    }
                }
            }
        }
    }
}

async fn wait_for_event(
    input: FlowerInputEventSource,
    etype: Flow3rInputEventType,
) -> Result<(), embassy_sync::pubsub::Error> {
    let mut sub = INPUTS_CHANNEL.subscriber()?;
    loop {
        match sub.next_message().await {
            WaitResult::Lagged(_) => (),
            WaitResult::Message(msg) => {
                if msg == (Flow3rInputEvent { input, etype }) {
                    return Ok(());
                }
            }
        }
    }
}

pub struct InputParts {
    pub sw1_left: Flow3rInput,
    pub sw1_center: Flow3rInput,
    pub sw1_right: Flow3rInput,
    pub sw2_left: Flow3rInput,
    pub sw2_center: Flow3rInput,
    pub sw2_right: Flow3rInput,
}

/// This struct represents a single direction (left, down, right) of one of the tri-state-switches.
pub struct Flow3rInput(FlowerInputEventSource, usize);

impl Flow3rInput {
    pub async fn wait_for_release(&mut self) -> Result<(), embassy_sync::pubsub::Error> {
        wait_for_event(self.0, Flow3rInputEventType::Press).await
    }

    pub async fn wait_for_press(&mut self) -> Result<(), embassy_sync::pubsub::Error> {
        wait_for_event(self.0, Flow3rInputEventType::Press).await
    }

    pub async fn wait_for_any(&mut self) -> Result<(), embassy_sync::pubsub::Error> {
        match select(
            wait_for_event(self.0, Flow3rInputEventType::Press),
            wait_for_event(self.0, Flow3rInputEventType::Release),
        )
        .await
        {
            Either::First(e) => e,
            Either::Second(e) => e,
        }
    }

    pub async fn is_pressed(&self) -> bool {
        let state = INPUT_STATE.lock().await;
        state[self.1]
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Flow3rInputEvent {
    input: FlowerInputEventSource,
    etype: Flow3rInputEventType,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum FlowerInputEventSource {
    SW1Left,
    SW1Press,
    SW1Right,
    SW2Left,
    SW2Press,
    SW2Right,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Flow3rInputEventType {
    Press,
    Release,
}

#[embassy_executor::task]
async fn input_logger() -> ! {
    let mut inputs_subscriber = INPUTS_CHANNEL.subscriber().unwrap();
    loop {
        let message = inputs_subscriber.next_message().await;
        match message {
            WaitResult::Message(msg) => println!("input {:?} activated", msg.input),
            WaitResult::Lagged(err) => println!("input logger lagged: {}", err),
        }
    }
}
