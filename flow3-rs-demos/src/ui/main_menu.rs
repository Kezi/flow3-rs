use embassy_futures::select::{select3, Either3};
use embassy_time::{Duration, Timer};
use embedded_graphics::{
    mono_font::{ascii::FONT_10X20, MonoTextStyle},
    pixelcolor::Rgb565,
    prelude::*,
    text::Text,
};

use esp_println::println;
use flow3_rs::{display::Display, Flow3r};

use crate::demo_tasks::{display_demo, imu_demo, captouch_demo};

pub async fn main_menu(mut flow3r: Flow3r) -> ! {
    let mut inputs = flow3r.take_inputs().split();
    let mut display = flow3r.take_display();
    // let mut imu = flow3r.take_imu();

    println!("main menu running");

    let apps = ["input_test", "imu_test", "captouch_test"];
    let mut selected = 0usize;

    display.set_backlight(100).unwrap();

    display
        .fill_solid(&display.bounding_box(), Rgb565::BLACK)
        .unwrap();
    let mut text = Text::with_alignment(
        apps[selected],
        Point { x: 120, y: 120 },
        MonoTextStyle::new(&FONT_10X20, Rgb565::WHITE),
        embedded_graphics::text::Alignment::Center,
    );
    text.draw(&mut display).unwrap();
    display.flush().await.unwrap();

    println!("drawn");

    loop {
        match select3(
            inputs.sw1_center.wait_for_press(),
            inputs.sw1_right.wait_for_press(),
            inputs.sw1_left.wait_for_press(),
        )
        .await
        {
            Either3::First(_) => {
                start_current_app(apps[selected], &mut display).await;
                display
                    .fill_solid(&display.bounding_box(), Rgb565::BLACK)
                    .unwrap();
                text.draw(&mut display).unwrap();
                display.flush().await.unwrap();
            }
            Either3::Second(_) => {
                let selected_new = (apps.len() - 1).min(selected + 1);
                if selected != selected_new {
                    play_transition_animation(&mut display, apps[selected_new], &mut text, false)
                        .await;
                }
                selected = selected_new;
            }
            Either3::Third(_) => {
                let selected_new = if selected > 0 { selected - 1 } else { selected };
                if selected != selected_new {
                    play_transition_animation(&mut display, apps[selected_new], &mut text, true)
                        .await;
                }
                selected = selected_new;
            }
        }
    }
}

async fn play_transition_animation<'a>(
    display: &mut Display,
    app: &'a str,
    text: &mut Text<'a, MonoTextStyle<'a, Rgb565>>,
    direction: bool,
) {
    let mut offset = 0i32;

    while offset < text.bounding_box().size.width as i32 / 2 + 120 {
        offset += 20;
        display
            .fill_solid(&display.bounding_box(), Rgb565::BLACK)
            .unwrap();
        text.translate(Point::new(if direction { offset } else { -offset }, 0))
            .draw(display)
            .unwrap();
        display.flush().await.unwrap();
        Timer::after(Duration::from_millis(1)).await;
    }

    text.text = app;

    let mut offset = text.bounding_box().size.width as i32 / 2 + 120;

    while offset > 0 {
        offset -= 20;
        display
            .fill_solid(&display.bounding_box(), Rgb565::BLACK)
            .unwrap();
        text.translate(Point::new(if direction { -offset } else { offset }, 0))
            .draw(display)
            .unwrap();
        display.flush().await.unwrap();
        Timer::after(Duration::from_millis(1)).await;
    }
}

async fn start_current_app(app: &str, display: &mut Display) {
    match app {
        "input_test" => display_demo(display).await,
        "captouch_test" => captouch_demo(display).await,
        _ => (),
    }
}
