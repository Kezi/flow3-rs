#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]

mod demo_tasks;
mod ui;

use demo_tasks::draw_start_screen;
use embassy_executor::Spawner;
use embassy_time::{Duration, Timer, Delay};

use esp_backtrace as _;
use esp_println::println;
use flow3_rs::Flow3r;
use flow3_rs_rt::start_runtime;
use hal::prelude::*;

use crate::ui::main_menu::main_menu;

#[entry]
fn runtime() -> ! {
    start_runtime(main)
}

#[embassy_executor::task]
async fn main(mut flow3r: Flow3r) -> ! {
    println!("started main");
    // draw_start_screen(&mut flow3r.take_display()).await;

    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(demo_tasks::leds_fade(flow3r.take_leds())).unwrap();

    println!("started led task");

    Timer::after(Duration::from_micros(100)).await;

    main_menu(flow3r).await
}
