#![no_std]
#![feature(type_alias_impl_trait)]

//! This crate provides a runtime implementation for the [`Flow3r`] based on [`embassy`] as the async runtime.
//! It takes care of initializing the hardware and starting the async runtime and all necessary background tasks.
//! 
//! If you want to get started quickly, use the [`flow3-rs-template`] to set up a minimal `flow3-rs-rt` project, by running:
//! ```
//! cargo generate --git https://git.flow3r.garden/flow3r/flow3-rs
//! ```
//! 
//! [`Flow3r`]: https://flow3r.garden
//! [`embassy`]: https://embassy.dev

mod runtime;
mod flow3r;
pub use runtime::start_runtime;